CREATE TABLE users (
    id          SERIAL PRIMARY KEY,
    name        TEXT NOT NULL UNIQUE,
    role        TEXT NOT NULL,
    age   	    INTEGER NOT NULL CHECK (age > 0 AND age < 130),
    state  	    TEXT NOT NULL
);

CREATE TABLE categories (
    id          SERIAL PRIMARY KEY,
    name        TEXT NOT NULL UNIQUE,
    description TEXT
);

CREATE TABLE products (
    id          SERIAL PRIMARY KEY, 
    name        TEXT NOT NULL,
    SKU         TEXT NOT NULL UNIQUE,
    price       TEXT NOT NULL,
    category    INTEGER REFERENCES categories (id) NOT NULL
);

CREATE TABLE buyrecord(
	id			SERIAL PRIMARY KEY,
	sid			TEXT NOT NULL,
	uid			INTEGER NOT NULL,
	pid			INTEGER NOT NULL,
	quantity    INTEGER NOT NULL,
	ptime       TEXT NOT NULL,
	price       INTEGER NOT NULL
);
