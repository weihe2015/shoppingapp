<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Shopping Cart</title>
</head>
<body>
<%@ page language="java" import="java.util.*" %>
<% 
	String role = (String)session.getAttribute("role");
	String name1 = request.getParameter("name");  // The name of selected product
	if(role == null){
		response.sendRedirect("pleaseLogin.html");
	} 
	else if(name1 == null) {
		response.sendRedirect("invalidrequest.html");
	} 
	else{
		String id = request.getParameter("productId");
		String name = request.getParameter("name");
		String sku = request.getParameter("sku");
		String price = request.getParameter("price");
		String category = request.getParameter("category");
		int quantity = 0;
		try {
			quantity = Integer.parseInt(request.getParameter("quantity"));
		} catch (Exception e) {
		}
		if (quantity > 0) {
			Vector<String> shoppingList = (Vector<String>)session.getAttribute("shoppingList");
			Vector<Integer> listQuantity = (Vector<Integer>)session.getAttribute("listQuantity");
			if (shoppingList == null) {
				shoppingList = new Vector<String>();
				listQuantity = new Vector<Integer>();
			}
			int shopId = shoppingList.indexOf(id);
			if (shopId == -1) {
				shoppingList.add(id);
				listQuantity.add(quantity);
			} else {
				quantity += listQuantity.get(shopId);
				listQuantity.set(shopId,quantity);
			}
			session.setAttribute("shoppingList",shoppingList);
			session.setAttribute("listQuantity",listQuantity);
		 	response.sendRedirect("productsbrowsing.jsp");
		}
		else {
			response.sendRedirect("orderErr.html");
		}
	}
%>
</body>
</html>