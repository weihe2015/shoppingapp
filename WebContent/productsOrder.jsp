<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Products Order Confirmation</title>
</head>
<body>
	<%@ page language="java" import="java.sql.*" %>
	<%@ page language="java" import="java.util.*" %>
	<%
	String name1 = request.getParameter("name");  // The name of selected product
	String role = (String)session.getAttribute("role");
	if(role == null){
		response.sendRedirect("pleaseLogin.html");
	}
	else if(name1 == null){
		response.sendRedirect("invalidrequest.html");
	}
	else {
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;

		String url = "jdbc:postgresql://localhost:5432/postgres";
		String username = "postgres";
		String password = "admin";
		Class.forName("org.postgresql.Driver");
		conn = DriverManager.getConnection(url,username,password);
		
		Vector<Integer> idV = new Vector<Integer>();
		Vector<String> nameV = new Vector<String>();
		st = conn.createStatement();
		rs = st.executeQuery("SELECT * FROM categories");
		while(rs.next()){
			idV.add(rs.getInt("id"));
			nameV.add(rs.getString("name"));
		}
		st.close();
		String id = request.getParameter("productId");
		String name = request.getParameter("name");
		String sku = request.getParameter("sku");
		String price = request.getParameter("price");
		String category = request.getParameter("category");
	%>
	<button onclick="window.location.href='productsbrowsing.jsp'">Back</button>
	<br><br>
	<table border="1">
		<tr>
			<th>Name</th>
			<th>Sku</th>
			<th>Price</th>
			<th>Category</th>
			<th>Quantity</th>
		</tr>
		<tr>
			<form action="shoppingcartUpdate.jsp" method="GET">
				<input type="hidden" value=<%=id%> name="productId">
				<input type="hidden" value=<%=name%> name="name">
				<input type="hidden" value=<%=sku%> name="sku">
				<input type="hidden" value=<%=price%> name="price">
				<input type="hidden" value=<%=category%> name="category">
			<td>
				<font><%=name%></font>
			</td>
			<td>
				<font><%=sku%></font>
			</td>
			<td>
				<font><%=price%></font>
			</td>
			<td>
				<font><%=category%></font>
			</td>
			<td>
				<input style="width:60px" type="number" name="quantity"/>
			</td>
			<td>
				<input type="submit" value="Order"/>
			</td>
			</form>
		</tr>
	</table>
	<%
	}
	%>
</body>
</html>