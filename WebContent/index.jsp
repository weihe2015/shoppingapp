<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home Page</title>
</head>
<body>
	
	<%
		String role = (String)session.getAttribute("role");
		if(role == null){
			response.sendRedirect("pleaseLogin.html");
		} else {
			%>
			Hello, <%=role %>: <%= session.getAttribute("username") %>
			<br><br>
			<%
			if(role.equals("Owner")){
				%>
				<button onclick="window.location.href='categories.jsp'">Categories</button>
				<br><br>
				<form action="products.jsp" method="post" name="cateAll">
					<input type="hidden" name="cate" value="-1"/>
					<input type="submit" value="Products"/>
				</form>
				<% 
			}
				%>
				<br>
				<button onclick="window.location.href='productsbrowsing.jsp'">Products Browse</button>
				<br><br>
				<button onclick="window.location.href='shoppingcart.jsp'">Shopping Cart</button>
				<% 
		}
	%>
</body>
</html>