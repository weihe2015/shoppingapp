<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Categories</title>
</head>
<body>
	<%@ page language="java" import="java.sql.*" %>
	
	<%
	String role = (String)session.getAttribute("role");
	if(role == null){
		response.sendRedirect("pleaseLogin.html");
	} else if(role.equals("Customer")){
		%>
			<font>This page is available to owners only</font>
		<%
	} else{
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		
		String url = "jdbc:postgresql://localhost:5432/postgres";
		String username = "postgres";
		String password = "admin";
			
	%>
	<button onclick="window.location.href='index.jsp'">Back</button>
	<br><br>
	<fieldset>
	<table border="1">
		<tr>
			<th>Name</th>
			<th>Description</th>
			<th colspan=2>Action</th>
		</tr>
		<tr>
			<form action="categoriesUpdate.jsp" method="post">
			<input type="hidden" value="0" name="id">
				<td>
					<input type="text" value="" name="name"/>
				</td>
			
				<td>
					<textarea cols='40' rows='5' value="" name="description"></textarea>
				</td>
				<td>
					<button type="submit" name="insert">Insert</button>
				</td>
			</form>
		</tr>
		<%
			try{
				Class.forName("org.postgresql.Driver");
				conn = DriverManager.getConnection(url,username,password);
				st = conn.createStatement();
				rs = st.executeQuery("SELECT DISTINCT categories.id, categories.name, categories.description, products.category FROM categories LEFT OUTER JOIN products ON categories.id=products.category");
				while(rs.next()){
					int id = rs.getInt("id");
					String name = rs.getString("name");
					String description = rs.getString("description");
					String cate = rs.getString("category");
					%>
					<tr>
					<form action="categoriesUpdate.jsp" method="post">
						<input type="hidden" value=<%=id%> name="id">
						<td>
							<input type="text" value=<%=name%> name="name"/>
						</td>
					
						<td>
							<textarea cols='40' rows='5' name="description"><%=description%></textarea>
						</td>
						<td>
						<% if(cate == null) {
						%>
							<button type="submit" name="delete">Delete</button>
						<% 
						}
						%>
							<button type="submit" name="update">Update</button>
						</td>
					</form>
					</tr>
					<%
				}
			} catch(SQLException e){
				out.println(e.getMessage());
			}
			finally{
				// Release resources in a finally block in reverse-order of their creation
				if(st != null){
					try{
						st.close();
					} catch(SQLException e){
						e.printStackTrace();
					}
					st = null;
				}
				
				if(conn != null){
					try{
						conn.close();
					} catch(SQLException e){
						e.printStackTrace();
					}
					conn = null;
				}
			}
	}
		%>
	</table>
	</fieldset>
</body>
</html>