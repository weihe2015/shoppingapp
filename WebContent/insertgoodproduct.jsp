<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New Product Confirmation</title>
</head>
<body>
	<% String role = (String)session.getAttribute("role");
	   String pro_name = (String)session.getAttribute("name");
	if(role == null){
		response.sendRedirect("pleaseLogin.html");
	} else if(role.equals("Customer")){
		%>
			<font>This page is available to owners only</font>
		<%
	} else if(pro_name == null){
		response.sendRedirect("invalidrequest.html");
	}
	else{ %> 
	
		<button onclick="window.location.href='products.jsp'">Back</button>	
		<br><br>
		   Hello, <%= session.getAttribute("username") %>
		<h4>Confirmation Page of inserting product:</h4>
			You have insert the following product: <br>
			name: <%= session.getAttribute("name") %> <br>
			sku: <%= session.getAttribute("sku") %> <br>
			price: <%= session.getAttribute("price") %> <br>
			category: <%= session.getAttribute("catename") %>
		<% 
		session.removeAttribute("name");
		session.removeAttribute("sku");
		session.removeAttribute("price");
		session.removeAttribute("catename");
		} 
	%>
</body>
</html>