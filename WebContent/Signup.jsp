<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sign Up</title>
</head>
<body>
<%@ page language="java" import="java.sql.*" %>
<%@ page language="java" import="java.util.*" %>

	<% 
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		String url = "jdbc:postgresql://localhost:5432/postgres";
		String username = "postgres";
		String password = "admin";
		
		try{
			// Registering Postgresql JDBC driver with the DriverManager
			Class.forName("org.postgresql.Driver");
			
			// Open a connection to the database using DriverManager
			conn = DriverManager.getConnection(url,username,password);
		
			String name = request.getParameter("username");
			String role = request.getParameter("role");
			String age = request.getParameter("age");
			String state = request.getParameter("state");
			
			session.setAttribute("signupname",name);
			session.setAttribute("signupage",age);
			
			if(name != null && age != null && !name.equals("") && !age.equals("")){				
	         	// Create the prepared statement
	         	// Then use it to insert into user table
	         	ps = conn.prepareStatement("INSERT INTO users (name,role,age,state) VALUES (?,?,?,?)");
	         	
	         	ps.setString(1,name);
	         	ps.setString(2,role);
	         	ps.setInt(3,Integer.parseInt(age));
	         	ps.setString(4,state);
	         	int result = 0;
	         	result = ps.executeUpdate();
		       	response.sendRedirect("thankyou.html");
	   			 // Close the PrepareStatement
	  	      	 ps.close();
	  		 
	  	    	 // Close the Connection
	  	      	 conn.close();
			}
			else{
				response.sendRedirect("SignupFail.jsp");
			}
		} catch(Exception E){
			response.sendRedirect("SignupFail.jsp");
		}
		finally{
			// Release resources in a finally block in reverse-order of their creation
			if(ps != null){
				try{
					ps.close();
				} catch(SQLException e){
					e.printStackTrace();
				}
				ps = null;
			}
			
			if(conn != null){
				try{
					conn.close();
				} catch(SQLException e){
					e.printStackTrace();
				}
				conn = null;
			}
		}
		
	%>


</body>
</html>