<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Products</title>
</head>
<body>
<%@ page language="java" import="java.sql.*" %>
<%
	String role = (String)session.getAttribute("role");
	String idurl = request.getParameter("id");
	if(role == null){
		response.sendRedirect("pleaseLogin.html");
	} 
	else if(idurl == null) {
		response.sendRedirect("invalidrequest.html");
	} 
	else {
		Connection conn = null;
		PreparedStatement ps = null;
		Statement st = null;
		ResultSet rs = null;
			
		String url = "jdbc:postgresql://localhost:5432/postgres";
		String username = "postgres";
		String password = "admin";
		
		try{
			Class.forName("org.postgresql.Driver");
			conn = DriverManager.getConnection(url,username,password);
			
			int id = Integer.parseInt(request.getParameter("id"));
			String name = request.getParameter("name");
			String sku = request.getParameter("sku");
			String price = request.getParameter("price");
			String cateString = request.getParameter("category");
			int category = Integer.parseInt(request.getParameter("category"));
			String cateid = request.getParameter("category");
			if(name == null || name.equals("")){
				session.setAttribute("errorReason","Product's name is not provided.");
				response.sendRedirect("productFail.jsp");
			} 
			else if (sku == null || sku.equals("")) {
				session.setAttribute("errorReason","Product's sku is not provided.");
				response.sendRedirect("productFail.jsp");
			} 
			else if (price == null || price.equals("")) {
				session.setAttribute("errorReason","Product's price is not provided.");
				response.sendRedirect("productFail.jsp");
			} 
			else if (Integer.parseInt(price) <= 0) {
				session.setAttribute("errorReason","Product's price needs to be greater than 0.");
				response.sendRedirect("productFail.jsp");
			}
			else {
				if(request.getParameter("update") != null){
					// Create the prepared statement
		         	// Then use it to insert into product table
		         	ps = conn.prepareStatement("UPDATE products SET name=?,sku=?,price=?,category=? WHERE id=?");
					ps.setString(1,name);
					ps.setString(2,sku);
					ps.setString(3,price);
					ps.setInt(4,category);
					ps.setInt(5,id);
				}
				else if(request.getParameter("insert") != null){
					ps = conn.prepareStatement("INSERT INTO products (name,sku,price,category) VALUES (?,?,?,?)");
					ps.setString(1,name);
					ps.setString(2,sku);
					ps.setString(3,price);
					ps.setInt(4,category);
				}
				else if(request.getParameter("delete") != null){
					ps = conn.prepareStatement("DELETE FROM products WHERE id=?");
					ps.setInt(1,id);
				}
				if(request.getParameter("insert") == null){
					int result = ps.executeUpdate();
					response.sendRedirect("products.jsp");
				}
				else if(request.getParameter("insert") != null){
					int result = ps.executeUpdate();
					session.setAttribute("name",name);
					session.setAttribute("sku",sku);
					session.setAttribute("price",price);
					st = conn.createStatement();
					rs = st.executeQuery("SELECT * FROM categories WHERE id='" + cateid + "'");
					String catename = "";
					while(rs.next()){
						catename = rs.getString("name");
					}
					session.setAttribute("catename",catename);
					response.sendRedirect("insertgoodproduct.jsp");
				}	
			}
		} catch(Exception e){
			response.sendRedirect("productFail.jsp");
		}
		finally{
			// Release resources in a finally block in reverse-order of their creation
			if(ps != null){
				try{
					ps.close();
				} catch(SQLException e){
					e.printStackTrace();
				}
				ps = null;
			}
			
			if(st != null){
				try{
					st.close();
				} catch(SQLException e){
					e.printStackTrace();
				}
				st = null;
			}
			
			if(conn != null){
				try{
					conn.close();
				} catch(SQLException e){
					e.printStackTrace();
				}
				conn = null;
			}
		}	
	}
	%>	
</body>
</html>