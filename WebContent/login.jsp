<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
</head>
<body>
<%@ page language="java" import="java.sql.*" %>
	<%
		Connection conn = null;
	    Statement st = null;
		
		String url = "jdbc:postgresql://localhost:5432/postgres";
		String username = "postgres";
		String password = "admin";
		
		try{
			Class.forName("org.postgresql.Driver");
			
			conn = DriverManager.getConnection(url,username,password);
			String name = request.getParameter("username");
			st = conn.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM users WHERE name='" + name + "'");
			
			if(!rs.next()){
				response.sendRedirect("loginFail.html");
			} 
			else {
			     //response.setCharacterEncoding("utf-8");  
			     //response.setHeader("iso-8859-1","utf-8"); 
			     
			    session.setAttribute("username",name);
			    session.setAttribute("userid", rs.getString("id"));
			    session.setAttribute("role", rs.getString("role"));
			 	response.sendRedirect("index.jsp");
			 	
	   			 // Close the PrepareStatement
	  	      	 st.close();
	  		 
	  	    	 // Close the Connection
	  	      	 conn.close();
			}
			
		} catch(Exception e){
			response.sendRedirect("loginFail.html");
		}
		finally{
			//System.out.println("In final");
			// Release resources in a finally block in reverse-order of their creation
			if(st != null){
				try{
					st.close();
				} catch(SQLException e){
					e.printStackTrace();
				}
				st = null;
			}
			
			if(conn != null){
				try{
					conn.close();
				} catch(SQLException e){
					e.printStackTrace();
				}
				conn = null;
			}
		}
	
	%>
</body>
</html>