<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Category Error</title>
</head>
<body>
<button onclick="window.location.href='categories.jsp'">Back</button>
<br><br>
<font>Illegal Operation!</font>
<%
	String err = (String)session.getAttribute("errorReason");
	if (err == null) {
		err = "";
	}
	session.removeAttribute("errorReason");
%>
<font><%=err%></font>
</body>
</html>