<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Payment Confirm</title>
</head>
<body>
	<%@ page language="java" import="java.sql.*" %>
	<%@ page language="java" import="java.util.*" %>
	<% 
	String role = (String)session.getAttribute("role");
	String card = request.getParameter("card");
	if(role == null){
		response.sendRedirect("pleaseLogin.html");
	} else if(card == null){
		response.sendRedirect("invalidrequest.html");
	}
	else {
		%>
		<button onclick="window.location.href='productsbrowsing.jsp'">Back</button>
		<br>
		<h3>Thank you for purchase!</h3>
		<h4>Here is you receipt:</h4>
		<table>
			<tr>
				<th>Name</th>
				<th>Sku</th>
				<th>Category</th>
				<th>Price</th>
				<th>Quantity</th>
				<th>Total Price</th>
			</tr>	
		<%
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
	
		String url = "jdbc:postgresql://localhost:5432/postgres";
		String username = "postgres";
		String password = "admin";
		Class.forName("org.postgresql.Driver");
		conn = DriverManager.getConnection(url,username,password);
		int userid = 0;
		
	try{
		String uid = (String)(session.getAttribute("userid"));
		userid = Integer.parseInt(uid);
	   }
		catch(Exception e){
		System.out.println(e.getMessage());
	}
		
		if (!card.matches("[0-9]+")) {
			response.sendRedirect("payErr.html");
		} else {
			Vector<Integer> idV = new Vector<Integer>();
			Vector<String> nameV = new Vector<String>();
			Vector<String> skuV = new Vector<String>();
			Vector<String> priceV = new Vector<String>();
			Vector<String> cateV = new Vector<String>();
			Vector<Integer> categoryIdV = new Vector<Integer>();
			Vector<String> categoryV = new Vector<String>();
			st = conn.createStatement();
			rs = st.executeQuery("SELECT * FROM categories");
			while(rs.next()){
				categoryIdV.add(rs.getInt("id"));
				categoryV.add(rs.getString("name"));
			}
			st.close();
			
			st = conn.createStatement();
			rs = st.executeQuery("SELECT * FROM products");
			while(rs.next()){
				idV.add(rs.getInt("id"));
				nameV.add(rs.getString("name"));
				skuV.add(rs.getString("sku"));
				priceV.add(rs.getString("price"));
				cateV.add(categoryV.get(categoryIdV.indexOf(rs.getInt("category"))));
			}
			st.close();
			
			Vector<String> shoppingList = (Vector<String>)session.getAttribute("shoppingList");
			Vector<Integer> listQuantity = (Vector<Integer>)session.getAttribute("listQuantity");
			if (shoppingList == null) {
				shoppingList = new Vector<String>();
				listQuantity = new Vector<Integer>();
			}
			double totalPrice = 0;
			PreparedStatement ps = null;
			String time = new java.util.Date().toString();
			String sid = (String)session.getId();
			for (int i = 0; i < shoppingList.size(); i++) {
				int productId = idV.indexOf(Integer.parseInt(shoppingList.get(i)));
				double subPrice = Double.parseDouble(priceV.get(productId)) * listQuantity.get(i);
				totalPrice += subPrice;
				ps = conn.prepareStatement("INSERT INTO buyrecord (sid,uid,pid,quantity,ptime,price) VALUES (?,?,?,?,?,?)");
				ps.setString(1,sid);
				ps.setInt(2,userid);
				ps.setInt(3,productId);
				ps.setInt(4,listQuantity.get(i));
				ps.setString(5,time);
				ps.setInt(6,Integer.parseInt(priceV.get(productId)));
				%>
					<tr>
						<td>
						<font><%=nameV.get(productId)%></font>
						</td>
						<td>
						<font><%=skuV.get(productId)%></font>
						</td>
						<td>
						<font><%=cateV.get(productId)%></font>
						</td>
						<td>
						<font><%=priceV.get(productId)%></font>
						</td>
						<td>
						<font><%=listQuantity.get(i)%></font>
						</td>
						<td>
						<font><%=subPrice%></font>
						</td>
					</tr>
				<%
				ps.executeUpdate();
			}
			session.removeAttribute("shoppingList");
			session.removeAttribute("listQuantity");
		%>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<th>Total:</th>
				<td>
				<font><%=totalPrice%></font>
				</td>
			</tr>
			<tr>
				<th>Pay with:</th>
				<td>
				<font><%=card%></font>
				</td>
			</tr>
		</table>
		<%
		} 
	}
		%>
	<br><br>
</body>
</html>