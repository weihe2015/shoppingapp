<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Product</title>
</head>
<body>
	<%@ page language="java" import="java.sql.*" %>
	<%@ page language="java" import="java.util.*" %>
	<% 
	String role = (String)session.getAttribute("role");
	if(role == null){
		response.sendRedirect("pleaseLogin.html");
	} 
	else {
			%>
			<div style="float:right">
				<a href="shoppingcart.jsp">Buy Shopping Cart</a>
			</div>
			<%
		Connection conn = null;
		Statement st = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
	
		String url = "jdbc:postgresql://localhost:5432/postgres";
		String username = "postgres";
		String password = "admin";
	
		try{
			Class.forName("org.postgresql.Driver");
			conn = DriverManager.getConnection(url,username,password);
			
			Vector<Integer> idV = new Vector<Integer>();
			Vector<String> nameV = new Vector<String>();
			st = conn.createStatement();
			rs = st.executeQuery("SELECT * FROM categories");
			while(rs.next()){
				idV.add(rs.getInt("id"));
				nameV.add(rs.getString("name"));
			}
			st.close();
	%>
	<button onclick="window.location.href='index.jsp'">Back</button>
	<br><br>
	<font>Search for product based on category or product name:</font><br><br>
	<table>
		<tr>
			<th>Category</th>
			<th>Product Name</th>
		</tr>
	</table>
	<form action="productsbrowsing.jsp" method="post">
		<input type="hidden" value="search" name="search">
			<select name="searchCategory">
				<option selected="selected" value="-1">All</option>
				<%
					for(int i=0; i<idV.size(); i++){
				%>
				<option value="<%=idV.get(i) %>"><%=nameV.get(i) %></option>
				<% 		
					}
				%>
			</select>
			<input type="text" value="" name="searchname">
			<button type="submit" name="search">Search</button>
	</form>
	<br>
	<table border="1">
		<tr>
			<th>Name</th>
			<th>Sku</th>
			<th>Price</th>
			<th>Category</th>
			<th colspan=1>Order</th>
		</tr>
		<% 
			st = conn.createStatement();
			rs = st.executeQuery("SELECT * FROM products");
			int cateId = -1;
			String searchName = "";
			try{
				searchName = request.getParameter("searchname");
				cateId = Integer.parseInt(request.getParameter("searchCategory"));
			} catch (Exception e) {
				searchName = "";
				cateId = -1;
			}
			while(rs.next()){
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String sku = rs.getString("sku");
				String price = rs.getString("price");
				int category = rs.getInt("category");
				if (cateId == -1 || category == cateId)
					if (name.indexOf(searchName) != -1) {
						%>
						<tr>
							<form action="productsOrder.jsp" method="post">
							<input type="hidden" value=<%=id%> name="productId">
							<input type="hidden" value=<%=name%> name="name">
							<input type="hidden" value=<%=sku%> name="sku">
							<input type="hidden" value=<%=price%> name="price">
							<input type="hidden" value=<%=nameV.get(idV.indexOf(category))%> name="category">
							<td>
								<font><%=name%></font>
							</td>
							<td>
								<font><%=sku%></font>
							</td>
							<td>
								<font><%=price%></font>
							</td>
							<td>
								<font><%=nameV.get(idV.indexOf(category))%></font>
							</td>
							<td>
								<button type="submit">Select</button>
							</td>	
							</form>
						</tr>
					<%
					}
			}
		} catch(SQLException e){
			 out.println(e.getMessage());
		}
		finally{
			// Release resources in a finally block in reverse-order of their creation
			if(st != null){
				try{
					st.close();
				} catch(SQLException e){
					e.printStackTrace();
				}
				st = null;
			}
			
			if(ps != null){
				try{
					ps.close();
				} catch(SQLException e){
					e.printStackTrace();
				}
				ps = null;
			}
			
			if(conn != null){
				try{
					conn.close();
				} catch(SQLException e){
					e.printStackTrace();
				}
				conn = null;
			}
		}
	}
		%>
	</table>
</body>
</html>