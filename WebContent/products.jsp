<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Products</title>
</head>
<body>
	<%@ page language="java" import="java.sql.*" %>
	<%@ page language="java" import="java.util.*" %>
	<%
	String role = (String)session.getAttribute("role");
	if(role == null){
		response.sendRedirect("pleaseLogin.html");
	}
	else if(role.equals("Customer")){
		%>
		<font>This page is available to owners only</font>
	<%
	} else{ 
	
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;

		String url = "jdbc:postgresql://localhost:5432/postgres";
		String username = "postgres";
		String password = "admin";
		Class.forName("org.postgresql.Driver");
		conn = DriverManager.getConnection(url,username,password);
		
		Vector<Integer> idV = new Vector<Integer>();
		Vector<String> nameV = new Vector<String>();
		st = conn.createStatement();
		rs = st.executeQuery("SELECT * FROM categories");
		while(rs.next()){
			idV.add(rs.getInt("id"));
			nameV.add(rs.getString("name"));
		}
		st.close();
		
	%>
	<div style="width:130px;position:absolute">
		<div style="border:1px solid black;margin-top:-1px">
			<form action="products.jsp" method="post" name="cateAll">
				<input type="hidden" name="cate" value="-1"/>
				<div style="width:130px" onClick="document.forms['cateAll'].submit()">		
					<div style="height:2px"></div>
					<div align="right" style="margin-right:5px">All</div>
					<div style="height:2px"></div>
				</div>
			</form>
		</div>
	<%
		for (int i = 0; i < idV.size(); i++) {
			%>
				<div style="border:1px solid black;margin-top:-1px">
					<form action="products.jsp" method="post" name="cate<%=i%>">
						<input type="hidden" name="cate" value="<%=idV.get(i)%>"/>
						<div style="width:130px" onClick="document.forms['cate<%=i%>'].submit()">
							<div style="height:2px"></div>
							<div align="right" style="margin-right:5px"><%=nameV.get(i)%></div>
							<div style="height:2px"></div>
						</div>
					</form>
				</div>
			<%
		}
	%>
	</div>
	<div style="left:170px;position:absolute">
	<button onclick="window.location.href='index.jsp'">Back</button>
	<br><br>
	<font>Search for product based on category:</font><br><br>
	<fieldset>
	<table>
		<tr>
			<th>Category</th>
			<th>Product Name</th>
		</tr>
	</table>
	<form action="products.jsp" method="post">
		<input type="hidden" value="-2" name="cate">
			<select name="searchCategory">
				<option selected="selected" value="-1">All</option>
				<%
					for(int i=0; i<idV.size(); i++){
				%>
				<option value="<%=idV.get(i) %>"><%=nameV.get(i) %></option>
				<% 		
					}
				%>
			</select>
			<input type="text" value="" name="searchname">
			<button type="submit" name="search">Search</button>
	</form>
	<br><font>Insert, Update or Delete Products:</font><br><br>
	<table border="1">
		<tr>
			<th>Name</th>
			<th>Sku</th>
			<th>Price</th>
			<th>Category</th>
			<th colspan=2>Action</th>	
		</tr>
		<tr>
			<form action="productsUpdate.jsp" method="post">
			<input type="hidden" value="0" name="id">
				<td>
					<input type="text" value="" name="name"/>
				</td>
				<td>
					<input type="text" value="" name="sku"/>
				</td>
				<td>
					<input type="text" value="" name="price"/>
				</td>
				<td>
					<select name="category">
					<option selected disabled hidden value=''></option>
					<%
						for(int i=0; i<idV.size();i++){
					%>
						<option value="<%= idV.get(i) %>"><%=nameV.get(i) %></option>
					<% 	
						}
					%>
					</select>
				</td>
				<td>
					<button type="submit" name="insert">Insert</button>
				</td>
			</form>
		</tr>
		<% 
		try{
			st = conn.createStatement();
			rs = st.executeQuery("SELECT * FROM products");
			int cateId = -1;
			try{
				cateId = Integer.parseInt(request.getParameter("cate"));
			} catch (Exception e) {
				cateId = -1;
			}
			while(rs.next()){
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String sku = rs.getString("sku");
				String price = rs.getString("price");
				int category = rs.getInt("category");
				if (cateId == -1 || category == cateId) {
				%>
		<tr>
			<form action="productsUpdate.jsp" method="post">
			<input type="hidden" value=<%=id%> name="id">
			<td>
				<input type="text" value=<%=name%> name="name">
			</td>
			<td>
				<input type="text" value=<%=sku%> name="sku">
			</td>
			<td>
				<input type="text" value=<%=price%> name="price">
			</td>
			<td>
				<select name="category">
				<%
					for(int i=0; i<idV.size();i++){
						if(category == idV.get(i)){
				%>
					<option selected="selected" value="<%=idV.get(i) %>"><%=nameV.get(i) %></option>
				<%
						}
					 else{
				%>
					<option value="<%=idV.get(i) %>"><%=nameV.get(i) %></option>
				<% 		
						}
					}		
				%>
				</select>
			</td>
			<td>
				<button type="submit" name="delete">Delete</button>
			</td>
			<td>	
				<button type="submit" name="update">Update</button>
			</td>	
			</form>
		</tr>
		<% 			}
					else if (cateId == -2) {
						int searchId = Integer.parseInt(request.getParameter("searchCategory"));
						String searchName = request.getParameter("searchname");
						if (searchId == -1 || searchId == category)
							if (name.indexOf(searchName) != -1) {
								%>
									<tr>
										<form action="productsUpdate.jsp" method="post">
										<input type="hidden" value=<%=id%> name="id">
										<td>
											<input type="text" value=<%=name%> name="name">
										</td>
										<td>
											<input type="text" value=<%=sku%> name="sku">
										</td>
										<td>
											<input type="text" value=<%=price%> name="price">
										</td>
										<td>
											<select name="category">
											<%
												for(int i=0; i<idV.size();i++){
													if(category == idV.get(i)){
											%>
												<option selected="selected" value="<%=idV.get(i) %>"><%=nameV.get(i) %></option>
											<%
													}
												 else{
											%>
												<option value="<%=idV.get(i) %>"><%=nameV.get(i) %></option>
											<% 		
													}
												}		
											%>
											</select>
										</td>
										<td>
											<button type="submit" name="delete">Delete</button>
										</td>
										<td>	
											<button type="submit" name="update">Update</button>
										</td>	
										</form>
									</tr>
								<%
							}
					}
				}
			} catch(SQLException e){
				out.println(e.getMessage());
			}
			finally{
				// Release resources in a finally block in reverse-order of their creation
				if(st != null){
					try{
						st.close();
					} catch(SQLException e){
						e.printStackTrace();
					}
					st = null;
				}
				
				if(conn != null){
					try{
						conn.close();
					} catch(SQLException e){
						e.printStackTrace();
					}
					conn = null;
				}
			}
	}
		%>

	</table>
	</fieldset>
	</div>
</body>
</html>