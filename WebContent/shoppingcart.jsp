<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Shopping cart</title>
</head>
<body>
	<%@ page language="java" import="java.sql.*" %>
	<%@ page language="java" import="java.util.*" %>
	
	<% 
	String role = (String)session.getAttribute("role");
	if(role == null){
		response.sendRedirect("pleaseLogin.html");
	}
	else {
	%>	
	<button onclick="window.location.href='index.jsp'">Back</button>
	<br>
	<h3>Here is your shopping Cart!</h3>
	<table>
		<tr>
			<th>Name</th>
			<th>Sku</th>
			<th>Category</th>
			<th>Price</th>
			<th>Quantity</th>
			<th>Total Price</th>
		</tr>
		<%
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
	
		String url = "jdbc:postgresql://localhost:5432/postgres";
		String username = "postgres";
		String password = "admin";
		Class.forName("org.postgresql.Driver");
		conn = DriverManager.getConnection(url,username,password);
		
		Vector<Integer> idV = new Vector<Integer>();
		Vector<String> nameV = new Vector<String>();
		Vector<String> skuV = new Vector<String>();
		Vector<String> priceV = new Vector<String>();
		Vector<String> cateV = new Vector<String>();
		Vector<Integer> categoryIdV = new Vector<Integer>();
		Vector<String> categoryV = new Vector<String>();
		st = conn.createStatement();
		rs = st.executeQuery("SELECT * FROM categories");
		while(rs.next()){
			categoryIdV.add(rs.getInt("id"));
			categoryV.add(rs.getString("name"));
		}
		st.close();
		
		st = conn.createStatement();
		rs = st.executeQuery("SELECT * FROM products");
		while(rs.next()){
			idV.add(rs.getInt("id"));
			nameV.add(rs.getString("name"));
			skuV.add(rs.getString("sku"));
			priceV.add(rs.getString("price"));
			cateV.add(categoryV.get(categoryIdV.indexOf(rs.getInt("category"))));
		}
		st.close();
		
		Vector<String> shoppingList = (Vector<String>)session.getAttribute("shoppingList");
		Vector<Integer> listQuantity = (Vector<Integer>)session.getAttribute("listQuantity");
		if (shoppingList == null) {
			shoppingList = new Vector<String>();
			listQuantity = new Vector<Integer>();
		}
		double totalPrice = 0;
		for (int i = 0; i < shoppingList.size(); i++) {
			int productId = idV.indexOf(Integer.parseInt(shoppingList.get(i)));
			double subPrice = Double.parseDouble(priceV.get(productId)) * listQuantity.get(i);
			totalPrice += subPrice;
			%>
				<tr>
					<td>
					<font><%=nameV.get(productId)%></font>
					</td>
					<td>
					<font><%=skuV.get(productId)%></font>
					</td>
					<td>
					<font><%=cateV.get(productId)%></font>
					</td>
					<td>
					<font><%=priceV.get(productId)%></font>
					</td>
					<td>
					<font><%=listQuantity.get(i)%></font>
					</td>
					<td>
					<font><%=subPrice%></font>
					</td>
				</tr>
			<%
		}
	%>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<th>Total:</th>
			<td>
			<font><%=totalPrice%></font>
			</td>
		</tr>
	</table>
	<br><br>
	<form name="payform" action="payConfirm.jsp" method="post">
		<font>Credit Card:</font>
		<input type="text" name="card"/>
		<input type="submit" value="Pay"/>
	</form>
	<%
		}
	%>
</body>
</html>