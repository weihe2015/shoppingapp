<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SignUp Fail</title>
</head>
<body>
<button onclick="window.location.href='Signup.html'">Back</button>
<br><br>
	<font>Sign up Fail!</font>
	<% 
	try{
		String name = (String)session.getAttribute("signupname");
		String age = (String)session.getAttribute("signupage");
		if(name == null || name.length() == 0){
			%>
			<br> <font>Name is not provided</font>
			<% 
		}
		else if (age == null || age.length() == 0){
			%>
			<br> <font>Age is not provided</font>
			<% 
		} 
		session.removeAttribute("signupname");
		session.removeAttribute("signupage");
	} catch(Exception e){
		System.out.println(e.getMessage());
	}
	%>
</body>
</html>