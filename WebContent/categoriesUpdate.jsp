<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Categories</title>
</head>
<body>
<%@ page language="java" import="java.sql.*" %>
	
<%
	String role = (String)session.getAttribute("role");
	String idurl = request.getParameter("id");
	if(role == null){
		response.sendRedirect("pleaseLogin.html");
	} 
	else if(idurl == null) {
		response.sendRedirect("invalidrequest.html");
	} 
	else{
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		String url = "jdbc:postgresql://localhost:5432/postgres";
		String username = "postgres";
		String password = "admin";
		
		try{	
			Class.forName("org.postgresql.Driver");
			conn = DriverManager.getConnection(url,username,password);
			int id = Integer.parseInt(request.getParameter("id"));
			String name = request.getParameter("name");
			String description = request.getParameter("description");
			if (name == null || name.equals("")) {
				session.setAttribute("errorReason","Category's name is not provided");
				response.sendRedirect("categoryFail.jsp");
			}
			else {
				if (request.getParameter("update") != null) {
					// Create the prepared statement
		         	// Then use it to insert into categories table
		         	ps = conn.prepareStatement("UPDATE categories SET name=?,description=? WHERE id=?");
		         	ps.setString(1,name);
		         	ps.setString(2,description);
		         	ps.setInt(3,id);
				} 
				else if (request.getParameter("insert") != null) {
					// Create the prepared statement
		         	// Then use it to insert into user table
			        ps = conn.prepareStatement("INSERT INTO categories (name,description) VALUES (?,?)");
			        ps.setString(1,name);
			        ps.setString(2,description);
				} 
				else if (request.getParameter("delete") != null) {
					ps = conn.prepareStatement("DELETE FROM categories WHERE id=?");
		         	ps.setInt(1,id);
				}
				int result = ps.executeUpdate();
				response.sendRedirect("categories.jsp");	
			}
		} catch(Exception e){
			response.sendRedirect("categoryFail.jsp");
		}
		finally{
			// Release resources in a finally block in reverse-order of their creation
			if(ps != null){
				try{
					ps.close();
				} catch(SQLException e){
					e.printStackTrace();
				}
				ps = null;
			}
			
			if(conn != null){
				try{
					conn.close();
				} catch(SQLException e){
					e.printStackTrace();
				}
				conn = null;
			}
		}	
	}
	%>
</body>
</html>